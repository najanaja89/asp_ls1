﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace asp_ls1.Models
{
    public static class HtmlExtension
    {
        public static MvcHtmlString ErrorLabel<TIn, TPropety>(
            this HtmlHelper html,
            Func<TIn model, TPropety> func) where TIn : class
        {
            var type = typeof(TIn);
            var propety = type.GetProperty(typeof(TPropety).Name);
            var validtaionAttrs = propety.GetCustomAttributes().ToList().Where(a => a.GetType().BaseType == typeof(ValidationAttribute)).Select(a => (ValidationAttribute)a);


            var p = new TagBuilder("p");
            foreach (var attr in validtaionAttrs)
            {
                p.InnerHtml = attr.ErrorMessage;
            }

            return new MvcHtmlString(p);
        }
    }
}