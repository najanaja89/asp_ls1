﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace asp_ls1.Models
{
    public class Product
    {
        public int Id { get; set; }

        [Required(ErrorMessage ="Need to fill")] //Аннотация    
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}