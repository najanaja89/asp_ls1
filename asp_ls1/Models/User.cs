﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace asp_ls1.Models
{
    public class User
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Login Needed")]
        [MinLength(3, ErrorMessage = "Minimum length 3 characters")]
        [MaxLength(30, ErrorMessage = "Maximum length 30 characters")]
        public string Login { get; set; }
       [Required(ErrorMessage = "Password Needed")]
        [MaxLength(30, ErrorMessage = "Maximum 30 characters")]
        [RegularExpression(@"^(?=.*\d{2})(?=.*[a-zA-Z]{2}).{8,}$", ErrorMessage = "Needed minimum 2 digits, minimum length 8 characters")]

        public string Password { get; set; }
    }
}